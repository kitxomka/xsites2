var app = angular.module('myApp', ['ui.router', 'ngAnimate', 'ngSanitize']);

app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/home');

    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: '/pages/home.html',
            controller: 'mainCtrl'
        })
        .state('portfolio', {
            url: '/portfolio',
            templateUrl: '/pages/portfolio.html',
            controller: 'mainCtrl'
        })
        .state('contact', {
            url: '/contact',
            templateUrl: '/pages/contact.html',
            controller: 'mainCtrl'
        });

}]);

//function showMore(showMoreId, containerId) {
//    var container = document.getElementById(containerId);
//    var showMoreLink = document.getElementById(showMoreId);
//
//    container.style.display = 'block';
//    showMoreLink.style.display = 'none';
//}
