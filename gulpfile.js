var gulp = require('gulp');
var plugins = require('gulp-load-plugins')({
    pattern: ['gulp-*']
});
var merge = require('merge-stream');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var cssmin = require('gulp-cssmin');
var rev = require('gulp-rev');
var util = require('gulp-util');
var imagemin = require('gulp-imagemin');



// DEV Tasks

gulp.task('devScripts', function() {
    return gulp.src('./src/scripts/**/*.js')
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(plugins.debug({title: 'js:'}))
        .pipe(plugins.concat('scripts.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./public/scripts'))
});

gulp.task('devSass', function() {
    return gulp.src('./src/styles/**/*.scss')
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(plugins.debug({title: 'sass:'}))
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./public/styles'))
});



// PROD Tasks

gulp.task('prodScripts', function() {
    return gulp.src('./src/scripts/**/*.js')
        .pipe(plugins.debug({title: 'prodScripts:'}))
        .pipe(plugins.concat('scripts.js'))
        .pipe(uglify())
        .pipe(plugins.rename('scripts.min.js'))
        .pipe(rev())
        .pipe(gulp.dest('./public/scripts'))
});

gulp.task('prodSass', function() {
    return gulp.src('./src/styles/**/*.scss')
        .pipe(plugins.debug({title: 'prodSass:'}))
        .pipe(sass().on('error', sass.logError))
        .pipe(cssmin())
        .pipe(plugins.rename({suffix: '.min'}))
        .pipe(rev())
        .pipe(gulp.dest('./public/styles'))
});
gulp.task('imgMin', function(){
    return gulp.src('src/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('public/img'))
});




// DEV + PROD Tasks

gulp.task('indexPugToHtml', function buildHTML() {
    return gulp.src('./src/index.pug')
        .pipe(plugins.pug())
        .pipe(gulp.dest('./public/'));
});

gulp.task('pugPagesToHtml', function(){
    return gulp.src('./src/pages/*.pug')
        .pipe(plugins.pug())
        .pipe(gulp.dest('./public/pages'));
});
gulp.task('copyVendorFiles', function() {
    gulp.src('src/css/bootstrap/*.css')
        .pipe(plugins.debug({title: 'copy-vendor-css:'}))
        .pipe(gulp.dest('public/vendor_styles/'));

});




var isProd = util.env.production;
if(isProd){
    gulp.task('default',['indexPugToHtml', 'prodSass', 'prodScripts', 'copyVendorFiles', 'imgMin', 'pugPagesToHtml'] ,function () {
        var target = gulp.src('./public/index.html');
        var sources = gulp.src(['./public/scripts/*.js', './public/styles/*.css', './public/vendor_styles/*.css'], {read: false});

        return target.pipe(plugins.inject(sources, {relative: true}))
            .pipe(gulp.dest('./public'));
    });
}else{
    gulp.task('default',['indexPugToHtml', 'devSass', 'devScripts', 'copyVendorFiles', 'imgMin', 'pugPagesToHtml'] ,function () {
        var target = gulp.src('./public/index.html');
        var sources = gulp.src(['./public/scripts/*.js', './public/styles/*.css', './public/vendor_styles/*.css'], {read: false});

        return target.pipe(plugins.inject(sources, {relative: true}))
            .pipe(gulp.dest('./public'));
    });
}

















